<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

function remove_html_pre($texte) {
	if (preg_match("#<html><pre>(.*?)<\/pre><\/html>#s", $texte, $m)) {
			return $m[1];
	}
	return $texte;
}

function faqmoi($texte){
	$entete="La petite question;La grande réponse\n";
	ecrire_fichier(_DIR_TMP."faq.txt",$entete . $texte);
	return "$texte" ;
}


function edgardmoi($texte){
	ecrire_fichier(_DIR_TMP."edgard.txt",$texte);
	return "$texte" ; 
}
